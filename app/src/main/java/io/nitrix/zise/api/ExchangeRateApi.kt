package io.nitrix.zise.api

import com.haroldadmin.cnradapter.NetworkResponse
import io.nitrix.zise.data.response.NetworkError
import retrofit2.http.GET

interface ExchangeRateApi {

    @GET("api/price")
    suspend fun getNexaExchangeRate(): NetworkResponse<Double, NetworkError>
}