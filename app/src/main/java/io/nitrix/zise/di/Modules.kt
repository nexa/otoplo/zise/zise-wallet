package io.nitrix.zise.di

import androidx.room.Room
import bitcoinunlimited.libbitcoincash.*
import com.haroldadmin.cnradapter.NetworkResponseAdapterFactory
import io.nitrix.zise.api.BlockchainApi
import io.nitrix.zise.api.ExchangeRateApi
import io.nitrix.zise.data.TransactionNoteDataBase
import io.nitrix.zise.repository.PinCodeRepository
import io.nitrix.zise.repository.ReceiveRepository
import io.nitrix.zise.repository.WalletRepository
import io.nitrix.zise.utils.SharedPreferenceUtil
import io.nitrix.zise.viewmodel.*
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { TransactionViewModel(get()) }
    viewModel { SendViewModel(get(), get()) }
    viewModel { ReceiveViewModel(get(), get()) }
    viewModel { AmountViewModel(get()) }
    viewModel { TransactionDetailsViewModel(get()) }
    viewModel { SettingsViewModel(get(), get()) }
    viewModel { PinCodeViewModel(get()) }
}

val repositoryModule = module {
    single {
        WalletRepository(
            context = get(),
            database = get(),
            transactionNoteDataBase = get(),
            exbitronApi = get(named(EXCHANGE_RATE_REPOSITORY)),
            blockchainApi = get(named(BLOCKCHAIN_INFO_REPOSITORY)),
            sharedPreferenceUtil = get()
        )
    }
    single { ReceiveRepository(get()) }
    single { PinCodeRepository(get()) }
}

val networkModule = module {
    single<Retrofit>(named(EXCHANGE_RATE_REPOSITORY)) {
        Retrofit.Builder()
            .baseUrl("https://mcap.nexaclub.org/")
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    single(named(EXCHANGE_RATE_REPOSITORY)) {
        get<Retrofit>(named(EXCHANGE_RATE_REPOSITORY)).create(
            ExchangeRateApi::class.java
        )
    }

    single<Retrofit>(named(BLOCKCHAIN_INFO_REPOSITORY)) {
        Retrofit.Builder()
            .baseUrl("https://explorer.nexa.org/")
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single(named(BLOCKCHAIN_INFO_REPOSITORY)) {
        get<Retrofit>(named(BLOCKCHAIN_INFO_REPOSITORY)).create(
            BlockchainApi::class.java
        )
    }
}

val dbModule = module {
    single { OpenKvpDB(get(), dbPrefix + "bip44walletdb") }
    single {
        Room.databaseBuilder(
            get(),
            TransactionNoteDataBase::class.java, TransactionNoteDataBase.DATABASE_NAME
        ).build()
    }
}

val utilsModule = module {
    single { SharedPreferenceUtil(androidContext()) }
    single { PlatformContext(androidContext()) }
}

const val EXCHANGE_RATE_REPOSITORY = "EXBITRON_REPOSITORY"
const val BLOCKCHAIN_INFO_REPOSITORY = "BLOCKCHAIN_INFO_REPOSITORY"