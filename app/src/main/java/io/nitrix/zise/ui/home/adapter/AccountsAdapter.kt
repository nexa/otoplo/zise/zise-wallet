package io.nitrix.zise.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import io.nitrix.zise.data.model.AccountItem
import io.nitrix.zise.ui.home.diffutil.AccountsDiffUtil
import io.nitrix.zise.ui.home.viewholder.AccountViewHolder
import io.nitrix.zise.ui.home.viewholder.InfoViewHolder

class AccountsAdapter(
    private val onClick: (AccountItem.Account) -> Unit = {}
) : ListAdapter<AccountItem, ViewHolder>(AccountsDiffUtil()) {

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position) is AccountItem.Account) {
            ITEM_TYPE_ACCOUNT
        } else ITEM_TYPE_INFO
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        when (viewType) {
            ITEM_TYPE_ACCOUNT -> AccountViewHolder(LayoutInflater.from(parent.context), parent)
            else -> InfoViewHolder(LayoutInflater.from(parent.context), parent)
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        if (holder is AccountViewHolder && item is AccountItem.Account) {
            holder.update(item, onClick)
        }
    }

    companion object {

        private const val ITEM_TYPE_INFO = 0
        private const val ITEM_TYPE_ACCOUNT = 1
    }
}