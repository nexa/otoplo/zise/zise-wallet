package io.nitrix.zise.ui.settings

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.databinding.FragmentSettingsBinding
import io.nitrix.zise.utils.NAVIGATION_FRAGMENT_RESULT
import io.nitrix.zise.viewmodel.SettingsViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


enum class SettingsNavDirection(val navDirections: NavDirections) {

    SECRET_PHRASE(SettingsFragmentDirections.actionSettingsFragmentToSecretPhraseFragment()),
    SET_PIN_CODE(SettingsFragmentDirections.actionSettingsFragmentToSetPinCodeFragment());

    val actionId: Int
        get() = navDirections.actionId
}


class SettingsFragment : Fragment(R.layout.fragment_settings) {

    private val viewBinding: FragmentSettingsBinding by viewBinding(FragmentSettingsBinding::bind)

    private val settingsViewModel by viewModel<SettingsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val resultStateFlow =
            findNavController().currentBackStackEntry?.savedStateHandle?.getStateFlow<Int?>(
                key = NAVIGATION_FRAGMENT_RESULT,
                null
            )

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                resultStateFlow?.collectLatest { result ->
                    when (result) {
                        SettingsNavDirection.SET_PIN_CODE.actionId -> {
                            findNavController().navigate(
                                SettingsNavDirection.SET_PIN_CODE.navDirections
                            )
                        }
                        SettingsNavDirection.SECRET_PHRASE.actionId -> {
                            findNavController().navigate(
                                SettingsNavDirection.SECRET_PHRASE.navDirections
                            )
                        }
                        else -> Unit
                    }
                }
            }
        }

        with(viewBinding) {
            val setPinStringId =
                if (settingsViewModel.isPinCodeSet) R.string.settings_change_your_pin
                else R.string.settings_set_your_pin

            setPinItem.text = getString(setPinStringId)
            setPinItem.setOnClickListener {
                if (settingsViewModel.isPinCodeSet) {
                    findNavController().navigate(
                        EnterPinCodeFragmentDirections.actionGlobalEnterPinCodeFragment(
                            SettingsNavDirection.SET_PIN_CODE.actionId
                        )
                    )
                } else {
                    findNavController().navigate(SettingsNavDirection.SET_PIN_CODE.navDirections)
                }
            }

            seedPhraseItem.setOnClickListener {
                if (settingsViewModel.isPinCodeSet) {
                    findNavController().navigate(
                        EnterPinCodeFragmentDirections.actionGlobalEnterPinCodeFragment(
                            SettingsNavDirection.SECRET_PHRASE.actionId
                        )
                    )
                } else {
                    findNavController().navigate(SettingsNavDirection.SECRET_PHRASE.navDirections)
                }
            }

            val requireContext = context ?: return
            downloadPaperItem.setOnClickListener {
                settingsViewModel.savePDF(requireContext)
            }
        }
    }
}