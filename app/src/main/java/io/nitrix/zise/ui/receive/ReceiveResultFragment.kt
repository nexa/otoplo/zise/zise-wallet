package io.nitrix.zise.ui.receive

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.databinding.FragmentReceiveResultBinding
import io.nitrix.zise.utils.createBitmapUri
import io.nitrix.zise.utils.textCopyThenPost
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat
import io.nitrix.zise.viewmodel.ReceiveScreenState
import io.nitrix.zise.viewmodel.ReceiveViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ReceiveResultFragment : Fragment(R.layout.fragment_receive_result) {

    private val viewBinding: FragmentReceiveResultBinding by viewBinding(
        FragmentReceiveResultBinding::bind
    )

    private val receiveViewModel by activityViewModel<ReceiveViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                receiveViewModel.receiveScreenFlow.collectLatest { updateReceiveScreenUI(it) }
            }
        }
        receiveViewModel.fetchResultAddressWithQRImage(
            minOf(
                viewBinding.qrCodeImage.layoutParams.width,
                viewBinding.qrCodeImage.layoutParams.height,
                1024
            ) + 200
        )

        viewBinding.copyButton.setOnClickListener { onCopyButtonClick() }
        viewBinding.shareButton.setOnClickListener {
            val shareIntent = createShareIntent()
            context?.startActivity(shareIntent)
        }
        viewBinding.receiveUriText.setOnLongClickListener {
            textCopyThenPost(
                requireContext(),
                receiveViewModel.receiveScreenFlow.value.address ?: ""
            )
            true
        }
    }

    private fun updateReceiveScreenUI(state: ReceiveScreenState) {
        if (!state.isLoading) {
            viewBinding.qrCodeImage.setImageBitmap(state.qrCode)
            viewBinding.receiveUriText.text = state.address
            viewBinding.requestTransactionAmountText.text = getString(
                R.string.value_in_nex,
                toCryptoFormat(state.amount)
            )
            viewBinding.requestTransactionExchangeText.text = getString(
                R.string.value_in_usd,
                toFiatFormat(state.usdAmount)
            )
        }
    }

    private fun onCopyButtonClick() {
        context?.let { context ->
            val clipboardManager =
                context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText(TRANSACTION_URI, viewBinding.receiveUriText.text)
            clipboardManager.setPrimaryClip(clipData)
            Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_LONG).show()
        }
    }

    private fun createShareIntent(): Intent {
        val bitmapUri = context?.let { context ->
            receiveViewModel.receiveScreenFlow.value.qrCode?.let { createBitmapUri(context, it) }
        }
        return Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, viewBinding.receiveUriText.text.toString())
            putExtra(Intent.EXTRA_STREAM, bitmapUri)
            type = "*/*"
        }, null)
    }

    companion object {

        private const val TRANSACTION_URI = "TRANSACTION_URI"
    }
}