package io.nitrix.zise.ui.settings.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import io.nitrix.zise.ui.settings.diffutil.SecretWordDiffUtil
import io.nitrix.zise.ui.settings.viewholder.SecretWordViewHolder

class SecretWordAdapter : ListAdapter<String, SecretWordViewHolder>(SecretWordDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SecretWordViewHolder(LayoutInflater.from(parent.context), parent)

    override fun onBindViewHolder(holder: SecretWordViewHolder, position: Int) {
        holder.update(getItem(position), position + 1)
    }
}