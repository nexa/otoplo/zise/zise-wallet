package io.nitrix.zise.ui.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.databinding.FragmentTransactionDetailsBinding
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat
import io.nitrix.zise.viewmodel.TransactionDetailsState
import io.nitrix.zise.viewmodel.TransactionDetailsViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.math.abs

class TransactionDetailsFragment : Fragment(R.layout.fragment_transaction_details) {

    private val viewBinding: FragmentTransactionDetailsBinding by viewBinding(FragmentTransactionDetailsBinding::bind)

    private val transactionDetailsViewModel by viewModel<TransactionDetailsViewModel>()

    private val args: TransactionDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                transactionDetailsViewModel.transactionDetailsStateFlow.collectLatest {
                    updateDetailsScreenUI(it)
                }
            }
        }

        context?.let { context ->
            viewBinding.exploreButton.setOnClickListener {
                val exploreIntent = createExploreIntent(args.transaction)
                context.startActivity(exploreIntent)
            }
            viewBinding.shareButton.setOnClickListener {
                val shareIntent = createShareIntent(args.transaction)
                context.startActivity(shareIntent)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        transactionDetailsViewModel.fetchTransaction(args.transaction)
    }

    override fun onStop() {
        super.onStop()
        val note = viewBinding.infoText.text.toString().trim()
        transactionDetailsViewModel.saveInfoForTransaction(note)
    }

    private fun updateDetailsScreenUI(state: TransactionDetailsState) {
        with(viewBinding) {
            transactionExchangeText.text = context?.getString(
                    if (state.exchangeAmount >= 0f)
                        R.string.positive_amount_text
                    else
                        R.string.negative_amount_text,
                 toFiatFormat(abs(state.exchangeAmount))
            )
            transactionAmountText.text = context?.getString(R.string.value_in_nex, toCryptoFormat(abs(state.amount)))

            networkFeesExchangeText.text = context?.getString(
                    if (state.exchangeFee >= 0f)
                        R.string.positive_amount_text
                    else
                        R.string.negative_amount_text,
                toFiatFormat(abs(state.exchangeFee))

            )
            networkFeesAmountText.text = context?.getString(R.string.value_in_nex, toCryptoFormat(abs(state.fee)))

            sendUriText.text = state.sendUri
            receiveUriText.text = state.receiveUri
            sendArrowImage.isInvisible = state.receiveUri.isNullOrEmpty()

            infoText.setText(state.info)
        }
    }

    private fun createExploreIntent(transactionId: String): Intent {
        return Intent().apply {
            action = Intent.ACTION_VIEW
            data = Uri.parse(BROWSER_BASE_URL + transactionId)
        }
    }

    private fun createShareIntent(transactionId: String): Intent {
        return Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, createExploreUri(transactionId).toString())
            type = "text/plain"
        }, null)
    }

    private fun createExploreUri(transactionId: String) : Uri {
        return Uri.parse(BROWSER_BASE_URL + transactionId)
    }

    companion object {

        private const val BROWSER_BASE_URL = "https://explorer.nexa.org/tx/"
    }
}