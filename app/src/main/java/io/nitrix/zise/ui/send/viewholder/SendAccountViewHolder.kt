package io.nitrix.zise.ui.send.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.CryptoType
import io.nitrix.zise.data.model.AccountForTransaction
import io.nitrix.zise.databinding.SendAccountItemLayoutBinding

class SendAccountViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.send_account_item_layout, parent, false)) {

    private val viewBinding: SendAccountItemLayoutBinding by viewBinding(SendAccountItemLayoutBinding::bind)

    fun update(
        item: AccountForTransaction,
        onClick: (AccountForTransaction) -> Unit = {}
    ) {
        with(itemView) {
            viewBinding.itemFrame.background =
                if (item.selected)
                    ContextCompat.getDrawable(context, R.drawable.home_header_background)
                else
                    ContextCompat.getDrawable(context, R.color.dark_blue)
            setOnClickListener { onClick(item) }
            viewBinding.accountText.text = when (item.cryptoType) {
                CryptoType.NEXA -> "NEX"
                CryptoType.BCH -> "BCH"
                CryptoType.XUSD -> "xUSD"
                CryptoType.XBTC -> "xBTC"
                CryptoType.XETH -> "xETH"
            }
        }
    }
}