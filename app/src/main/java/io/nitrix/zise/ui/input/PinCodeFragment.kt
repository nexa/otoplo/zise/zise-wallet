package io.nitrix.zise.ui.input

import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.InputSymbol
import io.nitrix.zise.databinding.FragmentPincodeBinding
import io.nitrix.zise.ui.decorations.KeyboardItemDecoration
import io.nitrix.zise.ui.input.adapter.InputAdapter
import io.nitrix.zise.utils.InputGridLayoutManager
import io.nitrix.zise.viewmodel.PinCodeViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class PinCodeFragment : Fragment(R.layout.fragment_pincode) {

    protected val viewBinding: FragmentPincodeBinding by viewBinding(FragmentPincodeBinding::bind)

    protected val pinCodeViewModel: PinCodeViewModel by viewModel()

    private val inputAdapter by lazy { InputAdapter(::onItemClick) }

    @get:StringRes
    abstract val titleId: Int

    abstract val resultCallback: (Boolean) -> Unit

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                pinCodeViewModel.pinCodeFlow.collectLatest {
                    setupPinCode(it)
                }
            }
        }

        viewBinding.pinCodeTextView.text = getString(titleId)

        viewBinding.confirmButton.setOnClickListener {
            resultCallback(pinCodeViewModel.isPinCodeCorrect)
        }

        with(viewBinding.inputRecyclerView) {
            addItemDecoration(
                KeyboardItemDecoration(
                    resources.getDimensionPixelSize(R.dimen.margin_12),
                    resources.getDimensionPixelSize(R.dimen.margin_32),
                    GRID_SIZE
                )
            )
            layoutManager = InputGridLayoutManager(
                context,
                GRID_SIZE
            ).apply {
                maxHeight =
                    resources.getDimensionPixelSize(R.dimen.input_amount_height)
            }
            adapter = inputAdapter
        }
        inputAdapter.submitList(INPUT_SYMBOLS_LIST)
    }

    protected fun showErrorBackground() {
        viewBinding.headerLayout.background = ContextCompat.getDrawable(requireContext(), R.drawable.header_background_animation)
        val animation = viewBinding.headerLayout.background as? AnimationDrawable
        animation?.setExitFadeDuration(1000)
        animation?.start()
    }

    private fun setupPinCode(list: List<String>) = with(viewBinding) {
        firstItem.background = getPinCodeItemResource(list.getOrNull(0))
        secondItem.background = getPinCodeItemResource(list.getOrNull(1))
        thirdItem.background = getPinCodeItemResource(list.getOrNull(2))
        fourthItem.background = getPinCodeItemResource(list.getOrNull(3))
    }

    private fun getPinCodeItemResource(string: String?): Drawable? {
        val resource = if (string.isNullOrEmpty()) {
            R.drawable.pincode_empty_item
        } else {
            R.drawable.pincode_fill_item
        }
        return ContextCompat.getDrawable(requireContext(), resource)
    }

    private fun onItemClick(item: InputSymbol) {
        val currentInput = pinCodeViewModel.getInput().toMutableList()
        when (item) {
            InputSymbol.POINT -> Unit
            InputSymbol.BLANK -> Unit
            InputSymbol.BACK -> {
                currentInput.removeLastOrNull()
                pinCodeViewModel.changeInput(currentInput)
            }
            else -> {
                currentInput.add(item.value)
                pinCodeViewModel.changeInput(currentInput)
            }
        }
    }

    companion object {

        private const val GRID_SIZE = 3

        private val INPUT_SYMBOLS_LIST = listOf(
            InputSymbol.ONE,
            InputSymbol.TWO,
            InputSymbol.THREE,
            InputSymbol.FOUR,
            InputSymbol.FIVE,
            InputSymbol.SIX,
            InputSymbol.SEVEN,
            InputSymbol.EIGHT,
            InputSymbol.NINE,
            InputSymbol.BLANK,
            InputSymbol.ZERO,
            InputSymbol.BACK,
        )
    }
}