package io.nitrix.zise.ui.input.diffutil

import androidx.recyclerview.widget.DiffUtil
import io.nitrix.zise.data.enumes.InputSymbol

class InputDiffUtil : DiffUtil.ItemCallback<InputSymbol>() {

    override fun areItemsTheSame(oldItem: InputSymbol, newItem: InputSymbol): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: InputSymbol, newItem: InputSymbol): Boolean {
        return oldItem == newItem
    }
}