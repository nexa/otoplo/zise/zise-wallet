package io.nitrix.zise.ui.input.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.InputSymbol
import io.nitrix.zise.databinding.InputItemLayoutBinding

class InputViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.input_item_layout, parent, false)) {

    private val viewBinding: InputItemLayoutBinding by viewBinding(InputItemLayoutBinding::bind)

    fun update(
        item: InputSymbol,
        onClick: (InputSymbol) -> Unit = {}
    ) {
        itemView.setOnClickListener { onClick(item) }
        viewBinding.inputButton.setImageResource(item.id)
    }
}