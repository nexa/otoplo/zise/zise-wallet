package io.nitrix.zise.ui.settings.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.databinding.SecretWordItemLayoutBinding

class SecretWordViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.secret_word_item_layout, parent, false)) {

    private val viewBinding: SecretWordItemLayoutBinding by viewBinding(SecretWordItemLayoutBinding::bind)

    fun update(
        item: String,
        position: Int
    ) {
        viewBinding.wordNumber.text = "$position."
        viewBinding.wordText.text = item
    }
}