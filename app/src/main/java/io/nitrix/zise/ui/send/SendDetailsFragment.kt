package io.nitrix.zise.ui.send

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.databinding.FragmentSendDetailsBinding
import io.nitrix.zise.ui.settings.EnterPinCodeFragmentDirections
import io.nitrix.zise.utils.NAVIGATION_FRAGMENT_RESULT
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat
import io.nitrix.zise.viewmodel.SendViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

enum class SendDetailsNavDirection {

    SEND_RESULT
}


class SendDetailsFragment : Fragment(R.layout.fragment_send_details) {

    private val viewBinding: FragmentSendDetailsBinding by viewBinding(FragmentSendDetailsBinding::bind)

    private val sendViewModel by activityViewModel<SendViewModel>()

    private val navArgs: SendDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val feePair = sendViewModel.getSuggestedFee()

        val amount = getString(
            R.string.value_in_nex,
            toCryptoFormat(sendViewModel.sendScreenFlow.value.amount + feePair.first)
        )
        val exchangeAmount = getString(
            R.string.value_in_usd,
            toFiatFormat(sendViewModel.sendScreenFlow.value.usdAmount + feePair.second)
        )

        if (navArgs.isSwaped) {
            viewBinding.sendAmountText.text = exchangeAmount
            viewBinding.sendExchangeText.text = amount
        } else {
            viewBinding.sendAmountText.text = amount
            viewBinding.sendExchangeText.text = exchangeAmount
        }

        viewBinding.networkFeesAmountText.text = getString(
            R.string.value_in_nex,
            toCryptoFormat(feePair.first)
        )
        viewBinding.networkFeesExchangeText.text = getString(
            R.string.value_in_usd,
            toFiatFormat(feePair.second)
        )
        if (sendViewModel.sendScreenFlow.value.url != "")
            viewBinding.sendUriText.text = sendViewModel.sendScreenFlow.value.url

        val resultStateFlow =
            findNavController().currentBackStackEntry?.savedStateHandle?.getStateFlow<Int?>(
                key = NAVIGATION_FRAGMENT_RESULT,
                null
            )

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                resultStateFlow?.collectLatest { result ->
                    if (result == SendDetailsNavDirection.SEND_RESULT.ordinal) {
                        sendTransaction()
                    }
                }
            }
        }

        viewBinding.sendButton.setOnClickListener {
            if (sendViewModel.isPinCodeSet) {
                findNavController().navigate(
                    EnterPinCodeFragmentDirections.actionGlobalEnterPinCodeFragment(
                        SendDetailsNavDirection.SEND_RESULT.ordinal
                    )
                )
            } else {
                sendTransaction()
            }
        }
    }

    private fun sendTransaction() {
        val info = viewBinding.infoText.text.toString()

        sendViewModel.send(
            note = info,
            callback = { isSuccess ->
                if (isSuccess) {
                    findNavController().navigate(
                        SendDetailsFragmentDirections.actionSendDetailsFragmentToSendResultFragment(
                            info,
                            navArgs.isSwaped
                        )
                    )
                }
            },
            errorCallback = { error ->
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
            }
        )
    }
}