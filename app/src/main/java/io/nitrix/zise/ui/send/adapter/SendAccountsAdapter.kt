package io.nitrix.zise.ui.send.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import io.nitrix.zise.data.model.AccountForTransaction
import io.nitrix.zise.ui.send.diffutil.SendAccountDiffUtil
import io.nitrix.zise.ui.send.viewholder.SendAccountViewHolder

class SendAccountsAdapter(
    private val onClick: (AccountForTransaction) -> Unit = {}
) : ListAdapter<AccountForTransaction, SendAccountViewHolder>(SendAccountDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SendAccountViewHolder(LayoutInflater.from(parent.context), parent)

    override fun onBindViewHolder(holder: SendAccountViewHolder, position: Int) {
        holder.update(getItem(position), onClick)
    }
}