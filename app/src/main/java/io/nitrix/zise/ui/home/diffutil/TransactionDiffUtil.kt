package io.nitrix.zise.ui.home.diffutil

import androidx.recyclerview.widget.DiffUtil
import io.nitrix.zise.data.model.Transaction

class TransactionDiffUtil : DiffUtil.ItemCallback<Transaction>() {

    override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
        return oldItem.title == newItem.title &&
                oldItem.exchange == newItem.exchange &&
                oldItem.amount == newItem.amount
    }
}