package io.nitrix.zise.ui.input.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import io.nitrix.zise.data.enumes.InputSymbol
import io.nitrix.zise.ui.input.diffutil.InputDiffUtil
import io.nitrix.zise.ui.input.viewholder.InputViewHolder

class InputAdapter(
    private val onClick: (InputSymbol) -> Unit = {}
) : ListAdapter<InputSymbol, InputViewHolder>(InputDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        InputViewHolder(LayoutInflater.from(parent.context), parent)

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        holder.update(getItem(position), onClick)
    }
}