package io.nitrix.zise.ui.send

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.databinding.FragmentSendResultBinding
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat
import io.nitrix.zise.viewmodel.SendViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class SendResultFragment : Fragment(R.layout.fragment_send_result) {

    private val viewBinding: FragmentSendResultBinding by viewBinding(FragmentSendResultBinding::bind)

    private val sendViewModel by activityViewModel<SendViewModel>()

    private val args: SendResultFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val feePair = sendViewModel.getFee()

        val amount = getString(
            R.string.value_in_nex,
            toCryptoFormat(sendViewModel.sendScreenFlow.value.amount + feePair.first)
        )
        val exchangeAmount = getString(
            R.string.value_in_usd,
            toFiatFormat(sendViewModel.sendScreenFlow.value.usdAmount + feePair.second)
        )

        if (args.isSwapped) {
            viewBinding.sendAmountText.text = exchangeAmount
            viewBinding.sendExchangeText.text = amount
        } else {
            viewBinding.sendAmountText.text = amount
            viewBinding.sendExchangeText.text = exchangeAmount
        }

        if (sendViewModel.sendScreenFlow.value.url != "")
            viewBinding.sendUriText.text = sendViewModel.sendScreenFlow.value.url
        viewBinding.exploreButton.setOnClickListener {
            val exploreIntent = createExploreIntent(sendViewModel.getTransactionId())
            context?.startActivity(exploreIntent)
        }
        viewBinding.shareButton.setOnClickListener {
            val shareIntent = createShareIntent(sendViewModel.getTransactionId())
            context?.startActivity(shareIntent)
        }

        viewBinding.networkFeesAmountText.text = getString(
            R.string.value_in_nex,
            toCryptoFormat(feePair.first)
        )
        viewBinding.networkFeesExchangeText.text = getString(
            R.string.value_in_usd,
            toFiatFormat(feePair.second)
        )

        viewBinding.infoText.setText(args.info)
    }

    private fun createExploreIntent(transactionId: String): Intent {
        return Intent().apply {
            action = Intent.ACTION_VIEW
            data = createExploreLink(transactionId)
        }
    }

    private fun createShareIntent(transactionId: String): Intent {
        return Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, createExploreLink(transactionId).toString())
            type = "text/plain"
        }, null)
    }

    override fun onStop() {
        super.onStop()
        val note = viewBinding.infoText.text.toString().trim()
        sendViewModel.saveInfoForTransaction(note)
    }

    private fun createExploreLink(transactionId: String) : Uri {
        return Uri.parse(BROWSER_BASE_URL + transactionId)
    }

    companion object {

        private const val BROWSER_BASE_URL = "https://explorer.nexa.org/tx/"
    }
}