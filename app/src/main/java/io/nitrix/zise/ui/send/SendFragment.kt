package io.nitrix.zise.ui.send

import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Size
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanIntentResult
import com.journeyapps.barcodescanner.ScanOptions
import io.nitrix.zise.R
import io.nitrix.zise.data.enumes.CryptoType
import io.nitrix.zise.data.model.AccountForTransaction
import io.nitrix.zise.databinding.FragmentSendBinding
import io.nitrix.zise.ui.decorations.HorizontalListItemDecoration
import io.nitrix.zise.ui.send.adapter.SendAccountsAdapter
import io.nitrix.zise.utils.QrCodeAnalyzer
import io.nitrix.zise.viewmodel.SendScreenState
import io.nitrix.zise.viewmodel.SendViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import timber.log.Timber

class SendFragment : Fragment(R.layout.fragment_send) {

    private val viewBinding: FragmentSendBinding by viewBinding(FragmentSendBinding::bind)

    private val sendViewModel by activityViewModel<SendViewModel>()

    private val sendAccountsAdapter by lazy { SendAccountsAdapter() }

    private val permissionLauncher = registerForActivityResult(RequestPermission()) {
        onPermissionResult(it)
    }

    private val scanLauncher = registerForActivityResult(ScanContract()) {
        onScanIntentResult(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                sendViewModel.sendScreenFlow.collectLatest { updateScreenUI(it) }
            }
        }

        context?.let { context ->
            with(viewBinding.sendAccountRecyclerView) {
                addItemDecoration(HorizontalListItemDecoration(resources.getDimensionPixelSize(R.dimen.margin_4)))
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = sendAccountsAdapter
                sendAccountsAdapter.submitList(listOf(AccountForTransaction(CryptoType.NEXA, true)))
            }

            if (!allPermissionsGranted()) {
                permissionLauncher.launch(REQUIRED_PERMISSIONS)
            } else {
                startPreview()
            }
            with(viewBinding.pasteButton) {
                setOnClickListener {
                    val manager =
                        context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = manager.primaryClip
                    if (clip != null) {
                        val text = clip.getItemAt(0)?.text.toString()
                        if (text.contains("nexa:")) {
                            sendViewModel.setSendingUrl(text)
                            sendViewModel.updateExchange()
                        } else {
                            Toast.makeText(context, "Incorrect URL", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(context, "Clipboard is empty", Toast.LENGTH_LONG).show()
                    }
                }
            }

            viewBinding.cameraButton.setOnClickListener {
                scanLauncher.launch(ScanOptions().apply {
                    setOrientationLocked(true)
                    setPrompt("Scan QR code")
                    setBeepEnabled(false)
                    setDesiredBarcodeFormats(ScanOptions.QR_CODE)
                })
            }
        }
    }

    private fun updateScreenUI(state: SendScreenState) {
        if (state.isExchangeUpdated) {
            findNavController().navigate(
                R.id.sendAmountFragment
            )
        }
    }

    private fun onPermissionResult(isGranted: Boolean) {
        if (isGranted) {
            startPreview()
        } else {
            Timber.d("Permissions didn't grant")
        }
    }

    private fun onScanIntentResult(scanResult: ScanIntentResult) {
        if (scanResult.contents != null) {
            val contents = scanResult.contents
            onScanResult(contents)
        }
    }

    private fun onScanResult(result: String) {
        if (result.contains("nexa")) {
            sendViewModel.setSendingUrl(result)
            sendViewModel.updateExchange()
        } else {
            Toast.makeText(context, "Incorrect URL", Toast.LENGTH_LONG).show()
        }
    }

    private fun startPreview() {
        context?.let { context ->
            val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
            cameraProviderFuture.addListener(
                {
                    val cameraProvider = cameraProviderFuture.get()

                    val preview = Preview.Builder()
                        .build()
                        .also { it.setSurfaceProvider(viewBinding.cameraPreview.surfaceProvider) }

                    val imageAnalysis = ImageAnalysis.Builder()
                        .setTargetResolution(
                            Size(
                                viewBinding.cameraPreview.width,
                                viewBinding.cameraPreview.height
                            )
                        )
                        .setBackpressureStrategy(STRATEGY_KEEP_ONLY_LATEST)
                        .build()
                    imageAnalysis.setAnalyzer(
                        ContextCompat.getMainExecutor(context),
                        QrCodeAnalyzer {
                            onScanResult(it)
                        }
                    )

                    val cameraSelector =
                        CameraSelector.Builder()
                            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                            .build()

                    try {
                        cameraProvider.unbindAll()
                        cameraProvider.bindToLifecycle(
                            viewLifecycleOwner,
                            cameraSelector,
                            preview,
                            imageAnalysis
                        )
                    } catch (exception: Exception) {
                        Timber.e(exception)
                    }
                },
                ContextCompat.getMainExecutor(context)
            )
        }
    }

    private fun allPermissionsGranted() = mutableListOf(REQUIRED_PERMISSIONS).all {
        ContextCompat.checkSelfPermission(
            requireContext(), it
        ) == PackageManager.PERMISSION_GRANTED
    }

    companion object {

        private const val REQUIRED_PERMISSIONS = android.Manifest.permission.CAMERA
    }
}