package io.nitrix.zise.ui.home.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import io.nitrix.zise.R
import io.nitrix.zise.data.model.Transaction
import io.nitrix.zise.databinding.TransactionItemLayoutBinding
import io.nitrix.zise.utils.toCryptoFormat
import io.nitrix.zise.utils.toFiatFormat
import kotlin.math.abs

class TransactionViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.transaction_item_layout, parent, false)) {

    private val viewBinding: TransactionItemLayoutBinding by viewBinding(TransactionItemLayoutBinding::bind)

    fun update(
        item: Transaction,
        onClick: (Transaction) -> Unit = {}
    ) {
        itemView.setOnClickListener { onClick(item) }
        viewBinding.transactionTitleText.text = item.title
        viewBinding.transactionAmountText.text =
                itemView.context.getString(R.string.value_in_nex, toCryptoFormat(abs(item.amount)))
        viewBinding.transactionExchangeText.setTextColor(
                ContextCompat.getColor(
                        itemView.context,
                        if (item.amount >= 0)
                            R.color.green_amount
                        else
                            R.color.red_amount
                )
        )
        viewBinding.transactionExchangeText.text = itemView.context.getString(
                if (item.exchange >= 0f)
                    R.string.positive_amount_text
                else
                    R.string.negative_amount_text,
                toFiatFormat(abs(item.exchange))
        )
    }
}