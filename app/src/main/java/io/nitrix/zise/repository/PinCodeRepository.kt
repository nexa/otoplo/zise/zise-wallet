package io.nitrix.zise.repository

import io.nitrix.zise.utils.SharedPreferenceUtil

class PinCodeRepository(
    private val sharedPreferenceUtil: SharedPreferenceUtil
) {

    var pinCode: String?
        get() = sharedPreferenceUtil.loadObject(PIN_CODE_ID, String::class.java)
        set(value) {
            value ?: return
            sharedPreferenceUtil.insertObject(PIN_CODE_ID, value)
        }

    var lastTry: Long
        get() = sharedPreferenceUtil.loadObject(LAST_TRY, Long::class.java) ?: 0
        set(value) = sharedPreferenceUtil.insertObject(LAST_TRY, value)

    var countTry: Int
        get() = sharedPreferenceUtil.loadObject(COUNT_TRY, Int::class.java) ?: 0
        set(value) = sharedPreferenceUtil.insertObject(COUNT_TRY, value)

    companion object {
        private const val PIN_CODE_ID = "PIN_CODE_ID"
        private const val LAST_TRY = "LAST_TRY"
        private const val COUNT_TRY = "COUNT_TRY"
    }
}