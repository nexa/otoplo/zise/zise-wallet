package io.nitrix.zise.repository

import bitcoinunlimited.libbitcoincash.*
import com.haroldadmin.cnradapter.NetworkResponse
import io.nitrix.zise.api.BlockchainApi
import io.nitrix.zise.api.ExchangeRateApi
import io.nitrix.zise.data.TransactionNoteDataBase
import io.nitrix.zise.data.model.AccountItem
import io.nitrix.zise.data.model.TransactionNote
import io.nitrix.zise.data.response.HeaderInfo
import io.nitrix.zise.data.response.NetworkError
import io.nitrix.zise.utils.SharedPreferenceUtil
import io.nitrix.zise.utils.fromSatoshiAmount
import timber.log.Timber
import java.math.BigInteger

data class UpdateExchangeState(
    val isSuccess: Boolean,
    val error: NetworkError?
)

class WalletRepository(
    private val context: PlatformContext,
    private val database: KvpDatabase,
    private val transactionNoteDataBase: TransactionNoteDataBase,
    private val exbitronApi: ExchangeRateApi,
    private val blockchainApi: BlockchainApi,
    private val sharedPreferenceUtil: SharedPreferenceUtil
) {

    private var wallet: Bip44Wallet? = null

    var exchange: Double
        get() = sharedPreferenceUtil.loadDouble(EXCHANGE_ID, 0.0)
        set(value) = sharedPreferenceUtil.insertObject(EXCHANGE_ID, value, true)

    private var createdWallet: Boolean
        get() = sharedPreferenceUtil.loadObject(CREATED_WALLET_ID, Boolean::class.java) ?: false
        set(value) = sharedPreferenceUtil.insertObject(CREATED_WALLET_ID, value, true)

    private var checkpointHeight: Long
        get() = sharedPreferenceUtil.loadObject(CHECKPOINT_HEIGHT_ID, Long::class.java) ?: 0L
        set(value) = sharedPreferenceUtil.insertObject(CHECKPOINT_HEIGHT_ID, value, true)

    suspend fun initWallet() {

        wallet = if (!createdWallet) {
            checkpointHeight = getCheckpointHeight()
            newWallet()
        } else {
            loadExistedWallet()
        }
        wallet?.let {
            startWallet(it, checkpointHeight)
            createdWallet = true
        }
    }

    private fun loadExistedWallet(): Bip44Wallet {
        return Bip44Wallet(wdb = database, name = WALLET_NAME)
    }

    private fun newWallet(): Bip44Wallet {
        return Bip44Wallet(
            wdb = database,
            name = WALLET_NAME,
            chainSelector = CHAIN_SELECTOR,
            wop = NEW_WALLET
        )
    }

    private suspend fun startWallet(wallet: Bip44Wallet, height: Long) {
        wallet.apply {
            val cnxnMgr = GetCnxnMgr(chain = chainSelector, name = name)

            val checkpointPriorHeader = getHeaderByHeight(height - 1)
            val checkpointPriorId = checkpointPriorHeader?.let {
                Hash256(it.hash)
            } ?: DEFAULT_CHECKPOINT_PRIOR_ID

            val checkpointHeader = getHeaderByHeight(height)
            val checkpointId = checkpointHeader?.let {
                Hash256(it.hash)
            } ?: DEFAULT_CHECKPOINT_ID
            val checkpointWork = checkpointHeader?.chainWork?.let {
                BigInteger(it.toByteArray())
            } ?: DEFAULT_CHAIN_WORK

            val chain = Blockchain(
                ChainSelector.NEXA,
                name,
                cnxnMgr,
                genesisBlockHash = GENESIS_BLOCK_ID,
                checkpointPriorBlockId = checkpointPriorId,
                checkpointId = checkpointId,
                checkpointHeight = height,
                checkpointWork = checkpointWork,
                context = context,
                dbPrefix = dbPrefix
            )

            prepareDestinations(2, 2)
            fillReceivingWithRetrieveOnly()

            addBlockchain(chain, height, START_PLACE)
            cnxnMgr.start()
            chain.start()
        }
    }

    fun getAccount(): AccountItem.Account? = wallet?.let {
        val walletBalance = it.balance + it.balanceUnconfirmed
        val balance = fromSatoshiAmount(walletBalance)
        return AccountItem.Account(
            name = it.name,
            amount = balance,
            exchangeAmount = balance * exchange
        )
    }

    fun restart() {
        wallet?.restart()
        wallet?.chainstate?.chain?.restart()
        wallet?.chainstate?.chain?.net?.restart()
    }

    suspend fun updateExchange(): UpdateExchangeState {
        return when (val response = exbitronApi.getNexaExchangeRate()) {
            is NetworkResponse.Success -> {
                exchange = response.body
                UpdateExchangeState(true, null)
            }
            is NetworkResponse.Error -> {
                Timber.e(response.error?.message)
                UpdateExchangeState(false, NetworkError(response.error?.message))
            }
        }
    }

    private suspend fun getHeaderByHeight(height: Long): HeaderInfo? {
        return when (val response = blockchainApi.getHeaderByHeight(height)) {
            is NetworkResponse.Success -> {
                response.body.first()
            }
            is NetworkResponse.Error -> {
                null
            }
        }
    }

    private suspend fun getCheckpointHeight(): Long {
        return when (val response = blockchainApi.getBlockchainInfo()) {
            is NetworkResponse.Success -> {
                response.body.height - 10
            }
            is NetworkResponse.Error -> {
                DEFAULT_CHECKPOINT_HEIGHT
            }
        }
    }

    fun getWallet() = wallet ?: throw Exception("Wallet isn't initialized")

    fun setOnWalletChange(onWalletChange: (Wallet) -> Unit) {
        wallet?.setOnWalletChange { onWalletChange(it) }
    }

    fun synced(): Boolean {
        return wallet?.synced() ?: false
    }

    fun send(amount: Long, address: PayAddress, note: String): Result<iTransaction?> {
        return kotlin.runCatching {
            wallet?.send(amount, address, sync = false, note = note)
        }
    }

    fun prepareSend(satoshiAmount: Long): iTransaction? {
        val output = wallet?.createChangeOutput(satoshiAmount) ?: return null
        return try {
            wallet?.prepareSend(mutableListOf(output))
        } catch (exception: Exception) {
            null
        }
    }

    fun abortTransaction(transaction: iTransaction) {
        wallet?.abortTransaction(transaction)
    }

    fun getTransactionsHistory(): List<TransactionHistory> {
        return wallet?.txHistory?.values?.toList() ?: emptyList()
    }

    fun getTransactionHistory(transactionId: String): TransactionHistory? {
        val hash = Hash256(hex = transactionId)
        return wallet?.txHistory?.values?.find { it.tx.id == hash }
    }

    fun saveNoteForTransaction(transactionNote: TransactionNote): Result<Unit> {
        return kotlin.runCatching {
            transactionNoteDataBase.transactionNoteDao().insert(transactionNote)
        }
    }

    fun getNoteForTransaction(transactionId: String): Result<String> {
        return kotlin.runCatching {
            transactionNoteDataBase.transactionNoteDao().get(transactionId)?.note ?: ""
        }
    }

    companion object {

        private const val WALLET_NAME = "Nexa"
        private const val EXCHANGE_ID = "EXCHANGE_ID"
        private const val START_PLACE = 1577836800L
        private const val DEFAULT_CHECKPOINT_HEIGHT = 6500L

        private const val CREATED_WALLET_ID = "CREATED_WALLET_ID"
        private const val CHECKPOINT_HEIGHT_ID = "CHECKPOINT_HEIGHT_ID"

        private val CHAIN_SELECTOR = ChainSelector.NEXA
        private val DEFAULT_CHECKPOINT_ID =
            Hash256("4121b1b8a7aa0456c0fd7f2684787d0473c053a2cf2a61949759cd3cee5b7c1b")
        private val DEFAULT_CHECKPOINT_PRIOR_ID =
            Hash256("a334c3b93b8e1c6c5a3379be407f034802be3b4f8e98c87738641d7308f6e56c")
        private val GENESIS_BLOCK_ID =
            Hash256("edc7144fe1ba4edd0edf35d7eea90f6cb1dba42314aa85da8207e97c5339c801")
        private val DEFAULT_CHAIN_WORK = 0x5c0efa47d0.toBigInteger()

    }
}