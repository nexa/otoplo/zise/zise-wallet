package io.nitrix.zise.repository

import com.haroldadmin.cnradapter.NetworkResponse
import io.nitrix.zise.api.BlockchainApi

class BlockchainRepository(
    private val blockchainApi: BlockchainApi,
) {

    suspend fun getHeight() : Result<Long> {
        return when (val response = blockchainApi.getBlockchainInfo()) {
            is NetworkResponse.Success -> {
                Result.success(response.body.height)
            }
            is NetworkResponse.Error -> {
                Result.failure(response.error ?: UnknownError())
            }
        }
    }

}