package io.nitrix.zise.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.TransactionHistory
import io.nitrix.zise.data.enumes.SortType
import io.nitrix.zise.data.model.Transaction
import io.nitrix.zise.repository.WalletRepository
import io.nitrix.zise.utils.fromSatoshiAmount
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class TransactionViewModel(
    private val walletRepository: WalletRepository
) : ViewModel() {

    private val transactionsList = mutableListOf<Transaction>()

    var sortType = SortType.BOTH

    private val _transactionsFlow = MutableStateFlow(emptyList<Transaction>())
    val transactionsFlow = _transactionsFlow.asStateFlow()

    fun fetchTransactions() = viewModelScope.launch(Dispatchers.IO) {
        val transactionsHistory = walletRepository.getTransactionsHistory()
            .sortedBy { it.date }
            .reversed()
            .map { convertToTransaction(it) }
            .filter { it.amount != 0.0 }
        when (sortType) {
            SortType.RECEIVED -> {
                _transactionsFlow.tryEmit(transactionsHistory.filter { it.amount > 0 })
            }
            SortType.BOTH -> {
                _transactionsFlow.tryEmit(transactionsHistory)
            }
            SortType.SENT -> {
                _transactionsFlow.tryEmit(transactionsHistory.filter { it.amount < 0 })
            }
        }
        transactionsList.clear()
        transactionsList.addAll(transactionsHistory)
    }

    private fun convertToTransaction(transaction: TransactionHistory): Transaction {
        val amount = fromSatoshiAmount(transaction.incomingAmt - transaction.outgoingAmt)
        val exchange = amount * walletRepository.exchange
        val info = getInfoForTransaction(transaction)
        return Transaction(
            id = transaction.tx.id.toHex(),
            title = info,
            amount = amount,
            exchange = exchange
        )
    }

    private fun getInfoForTransaction(transaction: TransactionHistory): String {
        walletRepository.getNoteForTransaction(transaction.tx.id.toHex())
            .onSuccess { note ->
                return note.ifEmpty { transaction.note }
            }.onFailure {
                return transaction.note
            }
        return EMPTY_NOTE
    }

    fun performSearch(searchString: String) {
        _transactionsFlow.tryEmit(transactionsList.filter {
            it.title.lowercase().contains(searchString)
        })
    }

    fun setSort(sortType: SortType) {
        this.sortType = sortType
        when (sortType) {
            SortType.RECEIVED -> {
                _transactionsFlow.tryEmit(transactionsList.filter { it.amount > 0 })
            }
            SortType.BOTH -> {
                _transactionsFlow.tryEmit(transactionsList)
            }
            SortType.SENT -> {
                _transactionsFlow.tryEmit(transactionsList.filter { it.amount < 0 })
            }
        }
    }

    companion object {

        private const val EMPTY_NOTE = ""
    }
}