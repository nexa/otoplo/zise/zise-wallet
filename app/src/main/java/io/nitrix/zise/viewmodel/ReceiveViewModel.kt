package io.nitrix.zise.viewmodel

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.nitrix.zise.data.response.NetworkError
import io.nitrix.zise.repository.ReceiveRepository
import io.nitrix.zise.repository.WalletRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

data class ReceiveScreenState(
    val isLoading: Boolean,
    val address: String?,
    val qrCode: Bitmap?,
    val amount: Double,
    val isExchangeUpdated: Boolean,
    val usdAmount: Double,
    val error: NetworkError?
)

class ReceiveViewModel(
    private val walletRepository: WalletRepository,
    private val receiveRepository: ReceiveRepository
) : ViewModel() {

    private val _receiveScreenFlow = MutableStateFlow(
        ReceiveScreenState(
            true,
            null,
            null,
            0.0,
            false,
            0.0,
            null
        )
    )
    val receiveScreenFlow = _receiveScreenFlow.asStateFlow()
    
    fun fetchInitialAddressWithQRImage(size: Int) = viewModelScope.launch(Dispatchers.Default) {
        val result = receiveRepository.getAddressWithQRCode(walletRepository.getWallet(), size)
        _receiveScreenFlow.emit(ReceiveScreenState(false, result.first, result.second, 0.0, false, 0.0, null))
    }

    fun refreshAddress(size: Int) = viewModelScope.launch(Dispatchers.Default) {
        val result = receiveRepository.refreshAddress(walletRepository.getWallet(), size)
        _receiveScreenFlow.emit(ReceiveScreenState(false, result.first, result.second, 0.0, false, 0.0, null))
    }

    fun fetchResultAddressWithQRImage(size: Int) = viewModelScope.launch(Dispatchers.Default) {
        _receiveScreenFlow.emit(
            ReceiveScreenState(
                true,
                _receiveScreenFlow.value.address,
                _receiveScreenFlow.value.qrCode,
                _receiveScreenFlow.value.amount,
                _receiveScreenFlow.value.isExchangeUpdated,
                _receiveScreenFlow.value.usdAmount,
                null
            )
        )
        val result = receiveRepository.getResultAddressWithQRCode(_receiveScreenFlow.value.amount, size)
        _receiveScreenFlow.emit(
            ReceiveScreenState(
                isLoading = false,
                address = result.first,
                qrCode = result.second,
                amount = _receiveScreenFlow.value.amount,
                isExchangeUpdated = _receiveScreenFlow.value.isExchangeUpdated,
                usdAmount = _receiveScreenFlow.value.usdAmount,
                error = null
            )
        )
    }

    fun setFiatAmount(fiatAmount: Double) {
        val amount = fiatAmount / walletRepository.exchange
        _receiveScreenFlow.tryEmit(
            ReceiveScreenState(
                isLoading = false,
                address = _receiveScreenFlow.value.address,
                qrCode = _receiveScreenFlow.value.qrCode,
                amount = amount,
                isExchangeUpdated = false,
                usdAmount = fiatAmount,
                error = null
            )
        )
    }

    fun updateExchange() = viewModelScope.launch(Dispatchers.IO) {
        _receiveScreenFlow.emit(
            ReceiveScreenState(
                isLoading = true,
                address = _receiveScreenFlow.value.address,
                qrCode = _receiveScreenFlow.value.qrCode,
                amount = _receiveScreenFlow.value.amount,
                isExchangeUpdated = _receiveScreenFlow.value.isExchangeUpdated,
                usdAmount = _receiveScreenFlow.value.usdAmount,
                error = null
            )
        )
        val result = walletRepository.updateExchange()
        _receiveScreenFlow.emit(
            ReceiveScreenState(
                isLoading = false,
                address = _receiveScreenFlow.value.address,
                qrCode = _receiveScreenFlow.value.qrCode,
                amount = _receiveScreenFlow.value.amount,
                isExchangeUpdated = true,
                usdAmount = _receiveScreenFlow.value.usdAmount,
                error = result.error
            )
        )
    }
}