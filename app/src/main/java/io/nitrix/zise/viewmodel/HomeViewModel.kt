package io.nitrix.zise.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.nitrix.zise.data.model.AccountItem
import io.nitrix.zise.repository.WalletRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class HomeViewModel(
    private val walletRepository: WalletRepository
) : ViewModel() {

    var isSyncing = true

    private val _accountsFlow = MutableStateFlow<AccountItem.Account?>(null)
    val accountsFlow = _accountsFlow.asStateFlow()

    fun initWallet() = viewModelScope.launch(Dispatchers.IO) {
        walletRepository.initWallet()
        walletRepository.updateExchange()

        _accountsFlow.tryEmit(walletRepository.getAccount())
    }

    fun subscribeOnWalletChange() {
        walletRepository.setOnWalletChange {
            _accountsFlow.tryEmit(walletRepository.getAccount())
        }
    }

    fun startSyncingWallet() = viewModelScope.launch(Dispatchers.Default) {
        isSyncing = true
        while (isSyncing) {
            checkWalletIsSynced()
            delay(5000)
        }
    }

    fun stopSyncingWallet() {
        isSyncing = false
    }

    private suspend fun checkWalletIsSynced() {
        val isSynced: Boolean = walletRepository.synced()
        _accountsFlow.emit(walletRepository.getAccount()?.copy(isSynced = isSynced))

        isSyncing = !isSynced
    }

    fun restart() {
        walletRepository.restart()
    }
}