package io.nitrix.zise.viewmodel

import android.os.CountDownTimer
import androidx.lifecycle.ViewModel
import io.nitrix.zise.repository.PinCodeRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.text.DecimalFormat
import java.util.*
import kotlin.math.pow
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds


class PinCodeViewModel(
    private val pinCodeRepository: PinCodeRepository
) : ViewModel() {

    private val _pinCodeFlow = MutableStateFlow<List<String>>(listOf())
    val pinCodeFlow = _pinCodeFlow.asStateFlow()

    private val _waitingTime = MutableStateFlow<String?>(null)
    val waitingTime = _waitingTime.asStateFlow()

    val isPinCodeCorrect: Boolean
        get() = checkPinCode(_pinCodeFlow.value)

    val isPinCodeSet: Boolean
        get() = !pinCodeRepository.pinCode.isNullOrEmpty()

    private var timer: CountDownTimer? = null

    fun changeInput(list: List<String>) {
        if (list.size > MAX_PIN_CODE_ITEMS) return
        _pinCodeFlow.tryEmit(list)
    }

    fun getInput(): List<String> {
        return _pinCodeFlow.value
    }

    private fun checkPinCode(list: List<String>): Boolean {
        val oldPinCode = pinCodeRepository.pinCode ?: return true

        val code = list.joinToString(separator = "")
        return code == oldPinCode
    }

    fun setErrorInput() {
        pinCodeRepository.countTry += 1
        pinCodeRepository.lastTry = Calendar.getInstance().timeInMillis
        if (pinCodeRepository.countTry > MAX_COUNT_TRY) {
            startTimer()
        }
    }

    fun resetErrorInput() {
        pinCodeRepository.countTry = 0
        pinCodeRepository.lastTry = 0
    }

    fun startTimer() {
        val secondsDelay = PIN_CODE_DELAY.pow(pinCodeRepository.countTry).seconds
        val lastTry = pinCodeRepository.lastTry.milliseconds

        val endDelay = secondsDelay.inWholeMilliseconds + lastTry.inWholeMilliseconds
        val currentTime = Calendar.getInstance().timeInMillis

        val millisInFuture = if (endDelay > currentTime) endDelay - currentTime else return

        stopTimer()

        timer = object : CountDownTimer(millisInFuture, COUNT_DOWN_INTERVAL) {

            val numberFormat = DecimalFormat("00")

            override fun onTick(millisUntilFinished: Long) {

                val minutes = millisUntilFinished / 60_000 % 60
                val seconds = millisUntilFinished / 1000 % 60
                val time = numberFormat.format(minutes) + ":" + numberFormat.format(seconds)
                _waitingTime.tryEmit(time)
            }

            override fun onFinish() {
                _waitingTime.tryEmit(null)
            }
        }

        timer?.start()
    }

    fun stopTimer() {
        timer?.cancel()
    }

    fun checkInput(list: List<String>): Boolean {
        return list == _pinCodeFlow.value
    }

    fun setPinCode() {
        pinCodeRepository.pinCode = getInput().joinToString(separator = "")
    }

    companion object {

        private const val PIN_CODE_DELAY = 2.0

        private const val COUNT_DOWN_INTERVAL = 1000L

        private const val MAX_PIN_CODE_ITEMS = 4
        private const val MAX_COUNT_TRY = 3
    }
}