package io.nitrix.zise.data.response

import com.google.gson.annotations.SerializedName

data class NexaTickerResponse(
    @SerializedName("at")
    val at: Long,
    @SerializedName("ticker")
    val ticker: TickerItemResponse
) {

    data class TickerItemResponse(
        @SerializedName("low")
        val low: Double,
        @SerializedName("high")
        val high: Double,
        @SerializedName("open")
        val open: Double,
        @SerializedName("last")
        val last: Double,
        @SerializedName("volume")
        val volume: Double,
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("vol")
        val vol: Double,
        @SerializedName("avg_price")
        val avgPrice: Double,
        @SerializedName("price_change_percent")
        val priceChangePercent: String,
        @SerializedName("at")
        val at: Long?,
    )
}
