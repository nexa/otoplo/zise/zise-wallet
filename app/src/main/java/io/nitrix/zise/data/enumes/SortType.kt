package io.nitrix.zise.data.enumes

enum class SortType { RECEIVED, BOTH, SENT }