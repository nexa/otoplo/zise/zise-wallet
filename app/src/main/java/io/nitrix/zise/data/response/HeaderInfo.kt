package io.nitrix.zise.data.response

import com.google.gson.annotations.SerializedName

typealias HeaderInfoResponse = Array<HeaderInfo>

data class HeaderInfo(
    @SerializedName("hash")
    val hash: String,
    @SerializedName("chainwork")
    val chainWork: String
)
