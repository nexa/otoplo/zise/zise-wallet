package io.nitrix.zise.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = Transaction.TABLE_NAME)
data class Transaction(
    @PrimaryKey
    @ColumnInfo(name = ID)
    val id: String,
    @ColumnInfo(name = TITLE)
    val title: String,
    @ColumnInfo(name = AMOUNT)
    val amount: Double,
    @ColumnInfo(name = EXCHANGE)
    val exchange: Double,
) : java.io.Serializable {
    companion object {

        const val TABLE_NAME = "transactions"
        const val ID = "id"
        const val TITLE = "title"
        const val AMOUNT = "amount"
        const val EXCHANGE = "exchange"
    }
}
