package io.nitrix.zise.data

import androidx.room.Database
import androidx.room.RoomDatabase
import io.nitrix.zise.data.model.TransactionNote

@Database(entities = [TransactionNote::class], version = 1)
abstract class TransactionNoteDataBase : RoomDatabase() {

    abstract fun transactionNoteDao(): TransactionNoteDao

    companion object {
        const val DATABASE_NAME = "database-transaction-note"
    }
}