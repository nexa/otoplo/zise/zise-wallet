package io.nitrix.zise.data.response

data class NetworkError(val message: String?)
