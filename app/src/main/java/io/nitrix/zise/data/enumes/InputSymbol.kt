package io.nitrix.zise.data.enumes

import io.nitrix.zise.R

enum class InputSymbol(val value: String, val id: Int) {
    ZERO("0", R.drawable.ic_zero),
    ONE("1", R.drawable.ic_one),
    TWO("2", R.drawable.ic_two),
    THREE("3", R.drawable.ic_three),
    FOUR("4", R.drawable.ic_four),
    FIVE("5", R.drawable.ic_five),
    SIX("6", R.drawable.ic_six),
    SEVEN("7", R.drawable.ic_seven),
    EIGHT("8", R.drawable.ic_eight),
    NINE("9", R.drawable.ic_nine),
    POINT(".", R.drawable.ic_point),
    BACK("", R.drawable.ic_back_number),
    BLANK("", R.color.transparent)
}