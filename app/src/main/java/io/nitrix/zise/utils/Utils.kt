package io.nitrix.zise.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import bitcoinunlimited.libbitcoincash.PlatformContext
import bitcoinunlimited.libbitcoincash.SATinMBCH
import bitcoinunlimited.libbitcoincash.currencyMath
import bitcoinunlimited.libbitcoincash.currencyScale
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import io.nitrix.zise.R
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DecimalFormat

import kotlin.math.roundToInt

@Throws(WriterException::class)
fun textToQREncode(
        context: PlatformContext,
        value: String,
        size: Int
): Bitmap? {
    val bitMatrix: BitMatrix

    val hintsMap = mapOf<EncodeHintType, Any>(
        EncodeHintType.CHARACTER_SET to "utf-8",
        EncodeHintType.MARGIN to 1
    )
    // //hintsMap.put(EncodeHintType.ERROR_CORRECTION, mErrorCorrectionLevel);
    try {
        bitMatrix = MultiFormatWriter().encode(value, BarcodeFormat.QR_CODE, size, size, hintsMap)
    } catch (e: IllegalArgumentException) {

        return null
    }


    val bitMatrixWidth = bitMatrix.getWidth()

    val bitMatrixHeight = bitMatrix.getHeight()

    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)


    //val white = 0xFFFFFFFF.toInt()
    //val black = 0xFF000000.toInt()
    val white: Int = context.let { ContextCompat.getColor(it.context, R.color.white) }
    val black: Int = context.let { ContextCompat.getColor(it.context, R.color.black) }

    var offset = 0
    for (y in 0 until bitMatrixHeight) {
        for (x in 0 until bitMatrixWidth) {
            pixels[offset] = if (bitMatrix.get(x, y))
                black
            else
                white
            offset += 1
        }
    }

    Timber.i("Encode image for $value")
    if (value.contains("Pay2")) {
        Timber.i("Bad image string")
    }
    val bitmap = Bitmap.createBitmap(pixels, bitMatrixWidth, bitMatrixHeight, Bitmap.Config.RGB_565)

    //bitmap.setPixels(pixels, 0, bitMatrixWidth, 0, 0, bitMatrixWidth, bitMatrixHeight)
    return bitmap
}

fun createBitmapUri(context: Context, bitmap: Bitmap): Uri? {
    val imagesFolder = File(context.cacheDir, "images")
    return try {
        imagesFolder.mkdirs()
        val file = File(imagesFolder, "shared_image.png")

        val stream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)
        stream.flush()
        stream.close()
        FileProvider.getUriForFile(context, "com.mydomain.fileprovider", file)

    } catch (exception: IOException) {
        Timber.e("IOException while trying to write file for sharing: " + exception.message)
        null
    }
}

fun textCopyThenPost(context: Context, textCopied: String) {
    val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    clipboardManager.setPrimaryClip(ClipData.newPlainText("", textCopied))
    Toast.makeText(context, "Copied", Toast.LENGTH_SHORT).show()
}

fun toSatoshiAmount(amount: Double): Long {
    val satoshiAmount = try {
        amount.toBigDecimal(currencyMath).setScale(currencyScale) * SATinBCH.toBigDecimal()
    } catch (e: Exception) {
        return 0L
    }
    return satoshiAmount.toLong()
}

fun fromSatoshiAmount(satoshiAmount: Long): Double {
    val currencyAmount = try {
        satoshiAmount.toBigDecimal(currencyMath).setScale(currencyScale) / SATinBCH.toBigDecimal()
    } catch (e: Exception) {
        return 0.0
    }
    return currencyAmount.toDouble()
}

fun toCryptoFormat(amount: Double): String {
    return cryptoFormat.format(amount.roundToInt())
}

fun toFiatFormat(amount: Double): String {
    return currencyFormat.format(amount)
}

fun toInputFormat(amount: Double): String {
    return inputFormat.format(amount)
}


private val currencyFormat = DecimalFormat("##,##0.00")
private val cryptoFormat = DecimalFormat("##,##0.#####")
private val inputFormat = DecimalFormat("##,##0.#####")

private val SATinBCH = SATinMBCH / 1000L
