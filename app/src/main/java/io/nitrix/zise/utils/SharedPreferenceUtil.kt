package io.nitrix.zise.utils

import android.content.Context
import com.google.gson.Gson
import java.io.Serializable

class SharedPreferenceUtil (val context: Context) {

    fun <T : Serializable> insertObject(id: String, any: T, persistent: Boolean = false) {
        insertObject(id, any, if (persistent) SS_PERSISTENCE_PREFS_NAME else SS_PREFS_NAME)
    }

    private fun <T : Serializable> insertObject(id: String, any: T, prefName: String) {
        context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
            .edit()
            .putString(id, Gson().toJson(any))
            .apply()
    }

    fun <T : Serializable> loadObject(id: String, cls: Class<T>) =
        loadObject(id, cls, SS_PERSISTENCE_PREFS_NAME) ?: loadObject(id, cls, SS_PREFS_NAME)

    private fun <T : Serializable> loadObject(id: String, cls: Class<T>, prefName: String): T? {
        return context.getSharedPreferences(prefName, Context.MODE_PRIVATE).getString(id, null).let {
            it?.let { Gson().fromJson(it, cls) }
        }
    }

    fun loadDouble(id: String, default: Double) =
        loadObject(id, Double::class.java) ?: default

    companion object {
        private const val SS_PREFS_NAME = "SS_PREFS_NAME"
        private const val SS_PERSISTENCE_PREFS_NAME = "SS_PERSISTENCE_PREFS_NAME"
    }
}