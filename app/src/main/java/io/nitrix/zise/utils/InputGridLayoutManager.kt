package io.nitrix.zise.utils

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class InputGridLayoutManager(val context: Context, spanCount: Int) :
    GridLayoutManager(context, spanCount) {

    var maxHeight: Int = width

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State?) {

        val layoutHeight = if (maxHeight <= height) maxHeight else height

        val rowCount = itemCount / spanCount

        for (i in 0 until itemCount) {
            val view = recycler.getViewForPosition(i)
            addView(view)
            val widthSpec =
                View.MeasureSpec.makeMeasureSpec(width / spanCount, View.MeasureSpec.EXACTLY)
            val heightSpec =
                View.MeasureSpec.makeMeasureSpec(layoutHeight / rowCount, View.MeasureSpec.EXACTLY)
            measureChildWithDecorationsAndMargin(view, widthSpec, heightSpec)
            val top = (i / spanCount) * (layoutHeight / rowCount)
            val bottom = (i / spanCount) * (layoutHeight / rowCount) + layoutHeight / rowCount
            val left = i % spanCount * width / spanCount
            val right = i % spanCount * width / spanCount + width / spanCount
            layoutDecoratedWithMargins(view, left, top, right, bottom)
        }
    }

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return RecyclerView.LayoutParams(
            RecyclerView.LayoutParams.MATCH_PARENT,
            RecyclerView.LayoutParams.WRAP_CONTENT
        )
    }

    override fun canScrollVertically(): Boolean {
        return false
    }

    override fun canScrollHorizontally(): Boolean {
        return false
    }

    private fun measureChildWithDecorationsAndMargin(
        view: View,
        widthSpec: Int,
        heightSpec: Int
    ) {
        val decorRect = Rect()
        calculateItemDecorationsForChild(view, decorRect)
        val layoutParams = view.layoutParams as RecyclerView.LayoutParams

        val decorLeft =
            if (decorRect.left == 0 && decorRect.right != 0) decorRect.right else decorRect.left
        val decorRight =
            if (decorRect.right == 0 && decorRect.left != 0) decorRect.left else decorRect.right
        val decorTop =
            if (decorRect.top == 0 && decorRect.bottom != 0) decorRect.bottom else decorRect.top
        val decorBottom =
            if (decorRect.bottom == 0 && decorRect.top != 0) decorRect.top else decorRect.bottom

        val updatedWidthSpec = updateSpecWithExtra(
            widthSpec,
            layoutParams.leftMargin + decorLeft,
            layoutParams.rightMargin + decorRight
        )
        val updatedHeightSpec = updateSpecWithExtra(
            heightSpec,
            layoutParams.topMargin + decorTop,
            layoutParams.bottomMargin + decorBottom
        )
        view.measure(updatedWidthSpec, updatedHeightSpec)
    }

    private fun updateSpecWithExtra(spec: Int, startInset: Int, endInset: Int): Int {
        if (startInset == 0 && endInset == 0) {
            return spec
        }
        val mode = View.MeasureSpec.getMode(spec)
        if (mode == View.MeasureSpec.AT_MOST || mode == View.MeasureSpec.EXACTLY) {
            return View.MeasureSpec.makeMeasureSpec(
                View.MeasureSpec.getSize(spec) - startInset - endInset,
                mode
            )
        }
        return spec
    }
}